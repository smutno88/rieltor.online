// header over backdrop with offcanvas
var myOffcanvas = document.getElementById('offcanvasMenu')
	lineIcons = document.querySelector('.menubtn__icon')
	
myOffcanvas.addEventListener('show.bs.offcanvas', function () {
  document.getElementById('mainHeader').style.zIndex = '1060';
  lineIcons.classList.add('typeclose');
})
myOffcanvas.addEventListener('hidden.bs.offcanvas', function () {
  document.getElementById('mainHeader').style.zIndex = '0';
  lineIcons.classList.remove('typeclose');
})

// Submenu Visiblity
var submenuBtn = document.querySelectorAll('.menu__item')
    submenuBody = document.querySelector('.submenu__content')
    submenuCont = document.querySelectorAll('.submenu__item')
for( let i = 0; i < submenuBtn.length; ++i) {
  const item = submenuBtn[i]
  item.addEventListener('mouseover', function(event){
    submenuBody.classList.add('show');
    for(i = 0; i < submenuCont.length; i++){
      submenuCont[i].classList.remove('visible');
    }
    let tabMenu = document.getElementById(item.dataset.itemmenu);
    tabMenu.classList.add('visible');
  });
}

var otherBody = document.querySelector('.offcanvas')
otherBody.addEventListener('mouseleave', function(){
  submenuBody.classList.remove('show');
});


//Selector
var selectB = document.querySelectorAll('.selector'),
    selectBtn = document.querySelectorAll('.selector__link')
selectBtn.forEach(button => {
  button.addEventListener('click', function(event) {
    selectB.forEach(item => {
      if(item.querySelector('.selector__link') !== button) {
        item.classList.remove('show')
      }
    })
    event._isClick = true
    button.parentElement.classList.toggle('show')
  })
})
document.body.addEventListener('click', function(event) {
  if(event._isClick == true || event.target.classList.contains('selector_link') == true || event.target.classList.contains('show') == true) 
  return
  selectB.forEach(item => {
    item.classList.remove('show')
  })
})

var filterValues = document.querySelectorAll('.filter__value')
filterValues.forEach(value => {
  value.addEventListener('click', function() {
    var valueText = value.innerText
    var parentValue = value.closest('.selector')
    parentValue.querySelector('.selector__link').innerHTML = valueText;
  })
})

// Selector End


// HomeSlider
const homeSlider = new Swiper('.swiperhome', {
    slidesPerView: 1,
    loop: true,
    navigation: {
        nextEl: ".arrow_next",
        prevEl: ".arrow_prev",
    },
    pagination: {
        el: ".swiperhome__paginationinner",
        clickable: true,
        type: 'custom',
        renderCustom: function (swiper, current, total) {
            let paginationHTML = '';
            paginationHTML += '<span class="pagination__number current__number">0'+current+'</span>';
            
            for (let i = 1; i <= total; i++){
                if(i == current) {
                    paginationHTML += '<span class="pagination__bullet active"></span>';
                } else {
                    paginationHTML += '<span class="pagination__bullet"></span>';
                }
            }
            
            paginationHTML += '<span class="pagination__number total__number">0'+total+'</span>';
            
            return paginationHTML;
        }
    },
});

// Blog Slider Home Page
const blogSlider = new Swiper ('.blog-swiper__wrap', {
    slidesPerView: 3,
    spaceBetween: 20,
    loop: true,
    navigation: {
        nextEl: ".blog-swiper__control .arrow_next",
        prevEl: ".blog-swiper__control .arrow_prev"
    },
    breakpoints: {
      // mobile + tablet - 320-990
      320: {
        slidesPerView: 1
      },
      // desktop >= 991
      991: {
        slidesPerView: 3
      }
    }
});

// Awards Slider
const awardSlider = new Swiper ('.awards-swiper__wrap', {
    slidesPerView: 4,
    spaceBetween: 20,
    loop: true,
    navigation: {
        nextEl: ".awards-swiper__control .arrow_next",
        prevEl: ".awards-swiper__control .arrow_prev"
    },
    breakpoints: {
      // mobile + tablet - 320-990
      320: {
        slidesPerView: 1
      },
      // desktop >= 991
      991: {
        slidesPerView: 4
      }
    }
});	

// Products Slider - Home Page
const prdouctSlider = new Swiper ('.product-swiper__wrap', {
    slidesPerView: 3,
    spaceBetween: 20,
    initialSlide: 6,
    grabCursor: true,
    centeredSlides: true,
    loop: true,
    navigation: {
        nextEl: ".products-swiper__control .arrow_next",
        prevEl: ".products-swiper__control .arrow_prev"
    },
    breakpoints: {
      // mobile + tablet - 320-990
      320: {
        slidesPerView: 1
      },
      // desktop >= 991
      991: {
        slidesPerView: 3
      }
    }
});


